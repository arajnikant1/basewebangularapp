import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate() {
         if (sessionStorage.getItem('isUserLoggedIn') == '1') {
             return true;
        }
        // tslint:disable-next-line:one-line
        else{
            this.router.navigate(['/user']);
            return false;
        }
    }
}
