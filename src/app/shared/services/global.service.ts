import { UserDetails } from './../../model/UserDetails';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
// validation patterns

public V_name: RegExp = /^[a-zA-Z][a-zA-Z ]{2,50}$/;
public V_email: RegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
public V_adhar: RegExp = /^\d{4}\s\d{4}\s\d{4}$/;
public V_pan: RegExp = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}$/;
public V_mob: RegExp = /^\d{10}$/;
public V_mobnum: RegExp = /^((\\+[1-12]{0,12}[ \\-]*)|(\\([0-12]{0,13}\\)[ \\-]*)|([0-12]{0,13})[ \\-]*)/;
public V_age: RegExp = /^\d{1,3}$/;
public V_acc: RegExp = /^\d{15}$/;
public V_hsn: RegExp = /^\d{8}$/;
public V_pin: RegExp = /^[1-9][0-9]{5}$/;
public V_num: RegExp = /^[0-9]*$/;
public V_price: RegExp = /^[1-9][0-9]*([.][0-9]{2}|)/;
public V_tim: RegExp = /^(10|11|12|13|14|15|16|17|18|19|20|21|22|23|[1-9]):[0-5][0-9]$/;
public V_minTenPer: RegExp = /^(10?[1-9]|[1-9][0-9]|100)$/;
public V_numNew: RegExp = /^(?:\d+|\d{1,3}(?:,\d{3})+)(?:(\.|,)\d+)?$/;
public V_PERCENT: RegExp = /^(0|[1-9]\d?)\.\d{4}|100\.0000$/;
public V_telephoneNum: RegExp = /^[1-9][0-9]{9,13}$/;
public V_gst: RegExp = /^([0-9][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;
public V_psd: RegExp = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!#^()-_+=%*?&]).{8,}/

userDetailsArrayNew: Array<UserDetails>;

  constructor() { }

  setUserDetailsData(userDetailsArray: Array<UserDetails>) {
    this.userDetailsArrayNew = userDetailsArray;
    console.log(this.userDetailsArrayNew);

  }

  getUserDetailsData() {
    return this.userDetailsArrayNew;
  }



}
