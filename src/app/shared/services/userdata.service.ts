import { Injectable } from '@angular/core';
import { UsersResponse } from 'src/app/model/Users/UsersResponse';

@Injectable({
  providedIn: 'root'
})
export class UserdataService {
  userData: UsersResponse = new UsersResponse();
  constructor() { }

  setUserData(userData: UsersResponse) {
    this.userData = userData;
  }

  getUserData() {
    return this.userData;
  }

  clearUserData() {
    this.userData = new UsersResponse();
  }
}
