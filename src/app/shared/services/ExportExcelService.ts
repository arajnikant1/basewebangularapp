
import { Http, ResponseContentType, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';

@Injectable()
export class ExportExcelService {
    btnDisable: boolean = true;
    constructor(public http: Http) { }

  //   getExcelInventoryHistoryByCode(typecode: number, companyId: number, pageNum, isExcel: boolean) {
  //       var urlForExcel = global.GET_ALL_INVENTORY_QUERY.concat("inventoryTypeCode:")
  //           .concat(typecode.toString()).concat(",purposeCode!2")
  //           .concat(",companyId:").concat(companyId.toString()).concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);
  //   }

  //   getExcelAllEnquiries( isExcel: boolean) {
  //       var urlForExcel = global.GET_ALL_FUNNEL_CUSTOMER.concat('?embed=enquiryHistory,followUpHistory').concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);
  //   }

  //   getExcelEnquiriesByStatus(isExcel:boolean, status: string, id: number){

  //   let isNew="";
  //   if(status != "NEW")
  //   {
  //     isNew = "&assignedToEmployeeId="+id;
  //   }
  //       var urlForExcel = global.GET_FUNNEL_CUSTOMER_BY_STATUS.concat(status).concat(isNew)
  //       .concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);
  //   }

  //   getExcelAllSales(isExcel:boolean){
  //       var urlForExcel = global.GET_ALL_LEAD_URL
  //       .concat('?embed=enquiry,leadHistory').concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);
  //   }
  //  // getExcelfilterCrm

  //  getExcelfilterSales(isExcel: boolean, searchQuery: string){
  //   var urlForExcel = global.GET_ALL_LEAD_FILTER
  //       .concat("?")
  //       .concat(searchQuery)
  //       .concat('&embed=enquiry,leadHistory')
  //       .concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);
  //  }

  //  getExcelSalesByStatus(isExcel: boolean, leadStatus:string, userId:number ){

  //   var urlForExcel = global.GET_ALL_LEAD_BY_STATUS.concat(leadStatus)
  //       .concat(status).concat('&assignedToEmployeeId='+userId)
  //       .concat("&embed=enquiry,leadHistory,followUpHistory")
  //       .concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);
  //  }

  //   getExcelfilterCrm( isExcel: boolean, searchQuery: string) {
  //       var urlForExcel = global.GET_FILTER_FUNNEL_CUSTOMER
  //       .concat("?isCompletedEnquiry:false,")
  //       .concat("&query=")
  //       .concat(searchQuery)
  //       .concat('&embed=enquiryHistory')
  //       .concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);
  //   }


  //   getExcelfilterComplaints(type:string,isExcel: boolean, searchQuery: string){
  //       // var urlForExcel = global.GET_ALL_CONNECTION_ISSUE_URL
  //       // .concat('?query=type:'+type+',')
  //       // .concat(searchQuery)
  //       // .concat('&embed=enquiry,leadHistory')
  //       // .concat('&excel=') + isExcel;
  //       // return this.getExcel(urlForExcel);

  //       // static getConnectionIssueFilter(type:string, searchQuery: string, pageNo: number, size: number) {
  //       //     const httpreq = new HttpRequest(global.GET_ALL_CONNECTION_ISSUE_URL
  //       //     .concat('?query=type:'+type+',')
  //       //     .concat(searchQuery)
  //       //     .concat("&size=").concat(size.toString())
  //       //     .concat("&page=").concat(pageNo.toString()));
  //       //   httpreq.classTypeValue = DisconnectionReportResponse;
  //       //   httpreq.taskcode = TaskCode.GET_CONNECTION_ISSUE_FILTER;
  //       //   return httpreq;
  //       //   }
  //   }

  //   getExcelAllComplaints(type:string, isExcel: boolean){
  //       var urlForExcel = global.GET_ALL_CONNECTION_ISSUE_URL
  //       .concat('?type='+type)
  //       .concat('&embed=statusHistory,followUpHistory').concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);
  //   }

  //   getExcelComplaintsByStatus(type:string, isExcel:boolean, status: string, id: number ){

  //           var urlForExcel = global.GET_CON_BY_STATUS
  //           .concat('?type='+type)
  //           .concat('&status='+status)
  //           .concat('&assignedToEmployeeId'+id)
  //           .concat('&embed=statusHistory,followUpHistory')
  //           .concat('&excel=') + isExcel;
  //           return this.getExcel(urlForExcel);
  //   }



  //   getExcelfilterInventoryHistoryCheckInByQuery(query: string, companyId: number, pageNum: string, isExcel: boolean, ) {
  //       var urlForExcel = global.FILTER_INVENTORY_BY_QUERY.concat(query)
  //           // .concat(",companyId:").concat(companyId.toString())
  //           .concat(",purposeCode!2")
  //           // .concat('&page=').concat(pageNum)
  //           .concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);

  //   }

  //   getExcelByAssignmentTypePage(compId: number, page: number, size: number, isExcel: boolean) {
  //       var urlForExcel = global.BY_ASSIGN_TYPE_FILTER
  //           .concat("companyId=").concat(compId.toString())
  //           .concat("&page=").concat(page.toString())
  //           .concat("&size=").concat(size.toString()).concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);

  //   }

  //   getCityExcel(isExcel: boolean) {
  //       var urlForExcel = global.GET_ALL_CITY_URL.concat('?excel=') + isExcel;
  //       return this.getExcel(urlForExcel);

  //   }

  //   getExcelByAssignmentFilter(query: string, page: number, size: number, isExcel: boolean) {
  //       var urlForExcel = global.BY_ASSIGN_TYPE_FILTER.concat(query).
  //           concat("&page=").concat(page.toString()).concat("&size=").concat(size.toString()).concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);

  //   }

  //   getExcelAllCompany(isExcel: boolean) {
  //       var isFromMainBranch: string;
  //       isFromMainBranch = sessionStorage.getItem('isFromMainBranch');
  //       var urlForExcel = global.GET_ALL_COMPANY_BRANCHES.concat('?all=').concat(isFromMainBranch).concat('&excel=') + isExcel;

  //       return this.getExcel(urlForExcel);
  //   }


  //   getExcelUserCompanyAndPageEmbed(companyId, pageNum, isExcel) {
  //       var isFromMainBranch: string;
  //       isFromMainBranch = sessionStorage.getItem('isFromMainBranch');

  //       var urlForExcel = global.GET_USERS_BY_PAGE.concat("?embed=inventoryCount,role,manager,company")
  //           .concat('&all=').concat(isFromMainBranch)
  //           .concat("&page=").concat(pageNum).concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);
  //   }

  //   getExcelUserCompanyAssigned(boolString, pageNum, isExcel) {
  //       var isFromMainBranch: string;
  //       isFromMainBranch = sessionStorage.getItem('isFromMainBranch');

  //       var urlForExcel = global.GET_ASSIGNED_EMPLOYEES
  //           .concat("?embed=inventoryCount,role,manager,company")
  //           .concat("&all=", isFromMainBranch)
  //           .concat("&page=").concat(pageNum).concat('&excel=') + isExcel;
  //       return this.getExcel(urlForExcel);

  //   }




  //   getStockTransferExcel(typecode, purposeCode, fromCompId) {
  //       let urlForExcel = global.GET_ALL_INVENTORY_QUERY.concat("inventoryTypeCode:")
  //           .concat(typecode.toString()).concat(",isCompleted:false").concat(",purposeCode:")
  //           .concat(purposeCode.toString())
  //           .concat('&embed=company,stockTransferEmployee,user,checkin')
  //           .concat(',fromCompanyId:').concat(fromCompId.toString())
  //           .concat('&excel=true')
  //           .concat('&typeCode=').concat('3');
  //       return this.getExcel(urlForExcel);
  //   }

  //   getStockTransferFilterExcel(typecode, purposeCode, fromCompId, query) {
  //       let urlForExcel = global.GET_ALL_INVENTORY_QUERY.concat("inventoryTypeCode:")
  //           .concat(typecode.toString()).concat(",isCompleted:false").concat(",purposeCode:")
  //           .concat(purposeCode.toString())
  //           .concat(',fromCompanyId:').concat(fromCompId.toString())
  //           .concat("," + query)
  //           .concat('&embed=company,stockTransferEmployee,user,checkin')
  //           .concat('&excel=true')
  //           .concat('&typeCode=').concat('3');
  //       return this.getExcel(urlForExcel);
  //   }

  //   // userAttendance/dates?from=27-02-2018&till=27-03-2018

  //   getUserAttendanceExcel(fromDate:string, tillDate:string) {
  //       let urlForExcel = global.GET_ALL_ATTENDANCE_EXCEL.concat(fromDate)
  //      .concat("&till=").concat(tillDate);

  //       return this.getExcel(urlForExcel);
  //   }
  //   getUserAttendanceExcelByManagerId(managerId:number, date:string) {
  //       let urlForExcel = global.GET_ALL_EMPLOYEE_BY_REPORTING_MANAGER
  //           .concat(managerId.toString()).concat("&from=")
  //           .concat(date)
  //           .concat("&excel=true") ;
  //       return this.getExcel(urlForExcel);
  //   }
  //   getUserAttendanceExcelFormat() {
  //       let urlForExcel = global.GET_ALL_EMPLOYEE_EXCEL_FORMAT;
  //       return this.getExcel(urlForExcel);
  //   }



    // get excel api
    getExcel(url) {
        console.log(url);
        let headers = new Headers();
        headers.set('token', sessionStorage.getItem('token'))
        return this.http.get(url, {
            headers: headers,
            responseType: ResponseContentType.Blob
        }).pipe(map(res => new Blob([res['_body']], { type: 'application/vnd.ms-excel' })));
    }





}


