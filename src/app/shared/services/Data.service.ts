import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { CommonService } from '../../framework/common.service';
import { BaseComponent } from '../../framework/BaseCompo';
import { HttpRequest } from '../../framework/HttpRequest';
import { TaskCode } from '../../framework/globals';
import { JsonParser } from '../../framework/ApiGenerator';
@Injectable()

export class DataService extends CommonService{
    protected commonService: CommonService;
    protected c: BaseComponent;
    constructor(http: Http) {
        // http : Http = new Http()
        super(http)
    }
    setBaseComponent(c: BaseComponent){
        this.c = c;
    }
    downloadData(req: HttpRequest) {
        this.onPreExecute(req.taskCode);
        this.callHttpReq(req)
            .subscribe(
            res => { if (res.error === true) { console.log(res); alert(res.message); } this.onResponseReceived(req.taskCode, res, req); },
            error => {
                console.log(error);
                this.onErrorReceived(req.taskCode); },
            () => { }
            );
    }
    onPreExecute(taskCode: TaskCode) {
        this.c.onPreExecute(taskCode);
    }
    onErrorReceived(taskCode: TaskCode) {
        console.log('onerrorrReceived of download data in data service class');
        alert('error in api calling with taskcode =' + taskCode);
        // this.c.onErrorReceived(taskCode);
    }
    onResponseReceived(taskCode: TaskCode, response: any, req: HttpRequest) {
        this.c.onResponseReceived(taskCode, this.parseJson(response, req));
    }

    parseJson(response: any, req: HttpRequest){
        return JsonParser.parseResponse(req.taskCode, response);
    }


    // throwError(error: any) {
    //     if (error !== '') {
    //         // alert("Server Error\n"+ error );
    //         console.log(error);
    //     }
    // }
}
