import { Pipe,PipeTransform } from "@angular/core";

//We tell Angular that this is a pipe by applying the @Pipe decorator which we import from the core Angular library.

@Pipe({

  //The @Pipe decorator takes an object with a name property whose value is the pipe name that we'll use within a template expression. It must be a valid JavaScript identifier. Our pipe's name is orderby.

  name: "orderby"
})

export class SortPipe implements PipeTransform {
  transform(array:Array<any>, args?, subargs?,sortType?) {

    console.log("Array is "+array);
    console.log("Args is "+args)
    console.log("Sort Type is " + sortType)
    // Check if array exists, in this case array contains articles and args is array property and sortType is ascending or descending

    if(array) {
        if(args)
        { if(subargs)
          {
            if (sortType == 'asc') {
              array.sort((a: any, b: any) => {
                if (a[args].subargs == b[args].subargs) {
                  return 0;
                } else {
                  if (a[args] > b[args]) {
                    return 1;
                  } else {
                    return -1;
                  }
                }
              });
              return array;
           }
            if (sortType == "desc") {
              array.sort((a: any, b: any) => {
                if (a[args].subargs == b[args].subargs) {
                  return 0;
                } else {
                  if (a[args] > b[args]) {
                    return -1;
                  } else {
                    return 1;
                  }
                }
              });
              return array;
            }
          }
          if (sortType == 'asc') {
             array.sort((a: any, b: any) => {
               if (a[args] == b[args]) {
                 return 0;
               } else {
                 if (a[args] > b[args]) {
                   return 1;
                 } else {
                   return -1;
                 }
               }
             });
             return array;
          }
           if (sortType == "desc") {
             array.sort((a: any, b: any) => {
               if (a[args] == b[args]) {
                 return 0;
               } else {
                 if (a[args] > b[args]) {
                   return -1;
                 } else {
                   return 1;
                 }
               }
             });
             return array;
           }
          

        return array;     //Same array returned

        }
        else
        {
        return array;   //Same array returned
        }

      

    }
    //
  }





}