
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from '../../../framework/BaseCompo';
import { CommonService } from '../../../framework/common.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  providers: [CommonService]
})
export class SidebarComponent extends BaseComponent implements OnInit {

  constructor(public service: CommonService ,public router: Router) {
    super(service)
   }

  ngOnInit() {

  }


  signOut() {
    sessionStorage.removeItem('isUserLoggedIn');
    sessionStorage.clear();
    localStorage.clear();
    this.router.navigate(['/user']);
  }




}
