import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
// import { BsDropdownModule } from 'ngx-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  localclick = false;
  dropDown= false;
  constructor(public router: Router, public globleService: GlobalService) { }

  ngOnInit() {
    console.log(this.globleService.getUserDetailsData());
    // alert(JSON.stringify(this.globleService.getUserDetailsData()));

  }

 dropDownList()
  {
  this.localclick = true;
  this.dropDown = !this.dropDown;
  }

  signOut()
  {
  sessionStorage.removeItem('token');
  sessionStorage.clear();
  localStorage.clear();
  this.router.navigate(['/login']);
  }




}
