import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GooglePlacesDirective } from './GooglePlacesDirective.directive';
import { TooltipDirective } from './tooltip.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [GooglePlacesDirective, TooltipDirective ],
  exports:[
      GooglePlacesDirective
  ]
})
export class DirectiveModule {}
