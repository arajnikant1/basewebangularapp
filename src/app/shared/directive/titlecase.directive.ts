import { Directive, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[appTitlecase]'
})
export class TitlecaseDirective {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  value: any;

  constructor() { }

  @HostListener('input', ['$event']) onInputChange($event) {
    this.value = $event.target.value.toUpperCase();
    this.ngModelChange.emit(this.value);
  }
}
