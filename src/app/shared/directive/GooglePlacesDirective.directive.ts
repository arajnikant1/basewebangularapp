import { Directive, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';

declare var google:any;

@Directive({
  selector: '[google-place]'
})
export class GooglePlacesDirective implements OnInit {
  @Output() onSelect: EventEmitter<any> = new EventEmitter();
  private element: HTMLInputElement;
  latitude;
  longitude;

  constructor(elRef: ElementRef) {
    //elRef will get a reference to the element where
    //the directive is placed
    this.element = elRef.nativeElement;
  }

  getFormattedAddress(place) {
    //@params: place - Google Autocomplete place object
    //@returns: location_obj - An address object in human readable format

   

    let location_obj = {};
    //console.log(place);
    //console.log(place.geometry.location.lat());
    //console.log(place.geometry.location.lng());
    for (let i in place.address_components) {
      let item = place.address_components[i];
      
      location_obj['addressLine1'] = place.formatted_address;// full address is here
      if(item['types'].indexOf("locality") > -1) {
        location_obj['city'] = item['long_name']//city is here
      }else if (item['types'].indexOf("administrative_area_level_2") > -1) {
        location_obj['district'] = item['long_name'] // district is here
      }else if (item['types'].indexOf("administrative_area_level_1") > -1) {
        location_obj['state'] = item['long_name'] // state is here
      } else if (item['types'].indexOf("street_number") > -1) {
        location_obj['flatNum'] = item['short_name'] // street with block with flat no is here
      }else if (item['types'].indexOf("sublocality_level_1") > -1) {
        location_obj['sector'] = item['long_name']// sector is here like dlf phase 4
      }else if (item['types'].indexOf("sublocality_level_2") > -1) {
        location_obj['phase'] = item['long_name']// phase is here like dlf phase 4
      }else if (item['types'].indexOf("sublocality_level_3") > -1) {
        location_obj['block'] = item['long_name']// block is here like c-block(its contains phase also)
      }else if (item['types'].indexOf("country") > -1) {
        location_obj['country'] = item['long_name']// country is here
      } else if (item['types'].indexOf("postal_code") > -1) {
        location_obj['pincode'] = item['short_name']// pin code is here
      } else if (item['types'].indexOf("route") > -1) {
        location_obj['galiNo_road'] = item['long_name']// road name etc is here
      }
      location_obj['latitude'] = place.geometry.location.lat();
      location_obj['longitude']= place.geometry.location.lng();
      ""
      // addressline2 = flatNum+galiNo_road+block+phase+sector+city+country+pincode
      
     
    }
    return location_obj;
  }

  ngOnInit() {
    const autocomplete = new google.maps.places.Autocomplete(this.element);


    //  //comment it if not proper
    //  const place: google.maps.places.PlaceResult = autocomplete.getPlace();

    //  // verify result
    //  if (place.geometry === undefined || place.geometry === null) {
    //    return;
    //  }
    //  // set latitude, longitude and zoom
    //  this.latitude = place.geometry.location.lat();
    //  this.longitude = place.geometry.location.lng();
 
    //  //comment till here



    //Event listener to monitor place changes in the input
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      //Emit the new address object for the updated place
      this.onSelect.emit(this.getFormattedAddress(autocomplete.getPlace()));
    });
  }

}