import { UserDetails } from './../../model/UserDetails';
import { GlobalService } from './../../shared/services/global.service';
import { Component, OnInit } from '@angular/core';
import { AppUtil } from 'src/app/framework/Utils/AppUtil';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  userDetail: UserDetails;
  constructor(public globalService: GlobalService) { }

  ngOnInit() {
    this.userDetail  = new UserDetails();
  }

  save() {
// console.log(this.userDetail);
if(this.validatData()){
  alert('valid data');
}


  }
  validatData(){
    if(AppUtil.isNotNull(this.userDetail)) {
      if(AppUtil.isNotNull(this.userDetail.age)){
        if(this.userDetail.age>50){
          alert('age cannot be grater than 50');
          return false;
        }
      }
    }
    return true;
  }

}
