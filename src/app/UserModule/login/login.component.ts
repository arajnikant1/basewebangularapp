import { CommonService } from './../../framework/common.service';
import { UserDetails } from './../../model/UserDetails';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../../shared/services/global.service';
import { HttpClient } from '@angular/common/http';
import { BaseComponent } from '../../framework/BaseCompo';
import { ApiGenerator } from '../../framework/ApiGenerator';
import { TaskCode } from '../../framework/globals';
import { LoginResponse } from '../../model/Login/LoginResponse';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseComponent implements OnInit {

  userDetails : UserDetails;

  userDetailsArray: Array<UserDetails>;

  constructor(public commonService: CommonService, public router: Router, public globalService: GlobalService, public http: HttpClient) {
    super(commonService);
   }

  ngOnInit() {
    this.userDetails = new UserDetails();
    this.userDetails.firstName = 'Abc';
    this.userDetails.lastName = 'Kumar';
    this.userDetails.email = 'abc@gmail.com';
    console.log(this.userDetails);

    this.userDetailsArray = new Array<UserDetails>();
    this.userDetailsArray.push(this.userDetails);
    this.userDetailsArray.push(this.userDetails);
    this.userDetailsArray.push(this.userDetails);
    this.userDetailsArray.push(this.userDetails);
    this.userDetailsArray.push(this.userDetails);
    this.userDetailsArray.push(this.userDetails);
    console.log(this.userDetailsArray);
    this.globalService.setUserDetailsData(this.userDetailsArray);

    // for (let index = 0; index < this.userDetailsArray.length; index++) {
    //   const element = this.userDetailsArray[index];

    // }
    // this.userDetailsArray.forEach(element => {

    // });

  }
  onLogin() {

    // this.http.get('https://jsonplaceholder.typicode.com/todos/1').subscribe(data => {
    //   console.log(data);
    //   console.log(JSON.stringify(data));
    // });

    var request =ApiGenerator.hitLoginApi();
    this.downloadData(request);
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const success = super.onResponseReceived(taskCode, response);
    if (success) {
      switch (taskCode) {
        case TaskCode.LOGIN_USER:
          const loginResponse = response as LoginResponse;
          alert(JSON.stringify(loginResponse));
          sessionStorage.setItem("isUserLoggedIn", '1');
    this.router.navigate(["/dashboard"]);
          break;
      }
    }
    return success;
  }



  goToRegister() {
    sessionStorage.setItem("isUserLoggedIn", '0');
    this.router.navigate(['/user/register']);
  }

  submit(event) {
    this.onLogin();

    // this.router.navigate(["/dashboard/users", 2]);
    // this.router.navigate(["/dashboard/rahul", 2, 'rahul']);
  }

}
