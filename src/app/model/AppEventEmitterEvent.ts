export class AppEventEmitterEvent {
    task: EmitterTask;
    eventData: any;
}

export enum EmitterTask {
  CLOSE_POPUP = 1,
  SAVE = 2,
  ADD = 3,
  UPDATE = 4,
  CLOSE_POPUP_EMAIL= 5,
  PROCESS_COMPLETD = 6,
  CLOSE_MAGNIFY_POPUP = 7,
  SAVE_MAGNIFY_POPUP = 8,
  PROCESS_CANCELLED = 9,
  SEND_EMAIL = 10,
  CLOSE_POPUP_CROSS = 11,

}
