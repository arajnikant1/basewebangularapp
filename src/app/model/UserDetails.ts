import { Type } from 'class-transformer';

export class TestData {
  abc: string;
  id: number;
}

export class UserDetails {
  mobilleNumber: number;
  firstName: string;
  lastName: string;
  email: string;
  age: number;
  @Type( () => TestData)
  dataArray: TestData[];  //Array<TestData>;
}


