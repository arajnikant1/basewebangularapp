export interface SearchInterface{
    filter(searchText: string):boolean
    getDisplayText():string;

}
