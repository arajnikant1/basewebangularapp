"use strict";
export const WEB_BASE_URL: string ="https://";
export const API_BASE_URL: string = WEB_BASE_URL + "jsonplaceholder.typicode.com";
export const value: string = "true";
export const GET_ALL_USERS_DATA: string = API_BASE_URL + "/users";
export const LOGIN_USER: string = API_BASE_URL + "/todos/1";

export enum TaskCode {
  GET_ALL_USERS_DATA,
  LOGIN_USER

}
