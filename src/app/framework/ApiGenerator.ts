
import { HttpRequest,HttpGenericRequest } from './HttpRequest';
import * as global from './globals';
import { classToPlain,plainToClass } from 'class-transformer';
import { ClassType } from 'class-transformer/ClassTransformer';
import { UsersResponse } from '../model/Users/UsersResponse';
import { LoginResponse } from '../model/Login/LoginResponse';


export class ApiGenerator {

  static hitLoginApi(): HttpRequest{
    const req = new HttpRequest(global.LOGIN_USER);
    // req.setGetMethod();
    req.classTypeValue = LoginResponse;
    req.taskCode = global.TaskCode.LOGIN_USER;
    return req;
  }

  static getAllUserData(): HttpRequest {
    const req = new HttpRequest(global.GET_ALL_USERS_DATA);
    req.classTypeValue = UsersResponse;
    req.taskCode = global.TaskCode.GET_ALL_USERS_DATA;
    return req;
  }

  // static savePackageDataWithPOQuantityEnable (methodId: number,data: any) {
  //   let req = new HttpRequest(URL.GET_METHOD_DATA + methodId);
  //   req.setPostMethod();
  //   req.params = classToPlain(data as Object)
  //   req.classTypeValue = MethodDataResponse;
  //   req.taskCode = TaskCode.SAVE_PO_ENABLE_STATUS;
  //   console.log(req.params);
  //   console.log(JSON.stringify(req.params))
  //   return req;
  // }

  // static loginUser (userName: string,password: string) {
  //   let req = new HttpRequest(URL.LOGIN);
  //   req.setPostMethod();
  //   req.headers.append('token','1234')
  //   req.params = { 'userName': userName,'password': password }
  //   req.classTypeValue = LoginResponse;
  //   req.taskCode = TaskCode.LOGIN;
  //   return req;
  // }

  // static deleteSubCriteria (packageId: number,subCriteriaId: number) {
  //   let req = new HttpRequest(URL.PATH_SUB_CRITERIA + packageId + '/' + subCriteriaId);
  //   req.setDeleteMethod();
  //   req.classTypeValue = MethodDataResponse;
  //   req.taskCode = TaskCode.DELETE_ITEM;
  //   return req;
  // }

  // static getDepartmentList (instId: number) {
  //   const http = new HttpRequest(URL.GET_DEPARTMENTS + instId);
  //   http.classTypeValue = DepartmentModel;
  //   http.taskCode = TaskCode.GET_DEPARTMENTS;
  //   http.isArrayResponse = true;
  //   return http;
  // }

}


export class JsonParser {
  static parseJson<T>(response: any,type: ClassType<T>): T {
    const parsedResponse = plainToClass(type,response as object);
    return parsedResponse;
  }

  static parseJsonString (response: any,type: ClassType<any>): any {
    const parsedResponse = plainToClass(type,response as object);
    return parsedResponse;
  }

  static parseJsonArray (response: any,type: ClassType<any>): any {
    const parsedResponse = plainToClass(type,response);
    return parsedResponse;
  }

  static parseResponse(taskCode: global.TaskCode, response: any) {
    switch (taskCode) {
      // case global.TaskCode.GET_CITY:
      //   return JsonParser.parseJson<any>(response, CitiesResponse);
      // case TaskCode.GET_AREAS:
      //   return JsonParser.parseJson<any>(response, AreaResponse);
    }
    return response;
  }
}
