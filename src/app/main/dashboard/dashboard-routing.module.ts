import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserdataService } from '../../shared/services/userdata.service';

const routes: Routes = [
  {path: '', redirectTo: 'users', pathMatch: 'full'},
  {path: 'users', component: DashboardComponent},
  {path: 'users/:id', component: DashboardComponent},
  {path: 'rahul/:id/:abc', component: DashboardComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [UserdataService]
})
export class DashboardRoutingModule { }
