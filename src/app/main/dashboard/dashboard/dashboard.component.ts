import { UserdataService } from './../../../shared/services/userdata.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersResponse } from '../../../model/Users/UsersResponse';
import { ApiGenerator } from '../../../framework/ApiGenerator';
import { TaskCode } from '../../../framework/globals';
import { AppUtil } from '../../../framework/Utils/AppUtil';
import { BaseComponent } from '../../../framework/BaseCompo';
import { CommonService } from '../../../framework/common.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends BaseComponent implements OnInit {
  usersData: Array<UsersResponse>;
  paramId: any;
  paramName: any;

  constructor(public service: CommonService, public router: Router, private route: ActivatedRoute,
              public userDataService: UserdataService) {
    super(service);
    // alert(this.route.snapshot.params.id);
    // console.log('this is url params ' + this.route.snapshot.params.id);


    // this.paramId = this.route.snapshot.params.id;
    // this.paramName = this.route.snapshot.params.abc;
   }


  ngOnInit() {
    this.getUserData();
  }

  getUserData() {
    this.downloadData(ApiGenerator.getAllUserData());
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    const success = super.onResponseReceived(taskCode, response);
    if (success) {
      switch (taskCode) {
        case TaskCode.GET_ALL_USERS_DATA:
          this.usersData = new Array<UsersResponse>();
          this.usersData = response as Array<UsersResponse>;
          if (!AppUtil.isListNullOrEmpty(this.usersData )) {
            this.userDataService.setUserData(this.usersData[0]);
          }
          break;
      }
    }
    return success;
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  arrayTwo(n: number): number[] {
    return [...Array(n).keys()];
  }

  arrayThree(n: number, startFrom: number): number[] {
    return [...Array(n).keys()].map(i => i + startFrom);
  }

}
