import { HeaderComponent } from './../shared/components/header/header.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from '../shared/components/sidebar/sidebar.component';
import { UserModule } from '../UserModule/user.module';

@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent,
    SidebarComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
  ]
})
export class MainModule { }
