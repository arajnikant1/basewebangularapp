import * as XLSX from 'xlsx';


export class ExcelUtil {
    static fileName: string
    static wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
    public static exportToExcel(filaName, tableName): void {
        this.fileName = filaName + ".xlsx"
        /* generate worksheet */
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(tableName);
        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
        /* save to file */
        XLSX.writeFile(wb, this.fileName, this.wopts);
    }
}
