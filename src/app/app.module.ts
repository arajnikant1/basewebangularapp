import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AuthGuard } from './shared/guard/auth.guard';
import { TitlecaseDirective } from './shared/directive/titlecase.directive';
import { SortPipe } from './shared/pipes/SortBy';
import { CommonService } from './framework/common.service';
import { GlobalService } from './shared/services/global.service';

@NgModule({
  declarations: [
    AppComponent,
    TitlecaseDirective,
    SortPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DragDropModule,
    HttpClientModule,
    HttpModule,
    FormsModule
  ],
  providers: [AuthGuard, CommonService, GlobalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
