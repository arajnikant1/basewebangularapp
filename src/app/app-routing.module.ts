import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
  {
  path: '',
  canActivate: [AuthGuard],
  // loadChildren: () => import('./main/main.module').then(m => m.MainModule)
  loadChildren: './main/main.module#MainModule',
  },
  {
  // loadChildren: () => import('./UserModule/user.module').then(m => m.UserModule)
  path: 'user', loadChildren: './UserModule/user.module#UserModule'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
